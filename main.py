from src.pdf_processing import extract_text_from_pdf
from src.language_processing import detect_language, lemmatize_text, load_harry_potter_words, clean_lemmatize_verify_words
from src.translation import translate_words
from API import api_key
import argparse
import re

import pandas as pd
from collections import Counter

###### Argument parser #######
# Positional arguments
parser = argparse.ArgumentParser(description="LearnPotter: A tool to learn new languages with Harry Potter")
parser.add_argument("nwords", type=int, help="Number of most common words to display", default=10)
# Non positional arguments
parser.add_argument("--file_path", help="Path to the PDF file to analyze", type=str, default="data/harry_potter.pdf")
parser.add_argument("--output", help="Path to save the most common words CSV file", type=str, default=None)

def main(file_path, api_key, nwords, output_file):
    try:
        # Step 1: Extract text from the PDF
        pdf_text = extract_text_from_pdf(file_path)

        # Step 2: Detect language of the text
        language = detect_language(pdf_text)
        print(f"Detected language: {language}")

        # Load Harry Potter-specific words for the detected language
        harry_potter_words = load_harry_potter_words(language)

        # Step 3: Lemmatize text
        lemmatized_text = lemmatize_text(pdf_text, language)

        # Step 4: Count word frequency
        word_counts = Counter(re.findall(r'\b\w+\b', lemmatized_text.lower()))

        # Step 5: Clean, lemmatize, and verify words
        cleaned_word_counts = clean_lemmatize_verify_words(word_counts, harry_potter_words)

        # Get the 200 most common words
        most_common_words = dict(cleaned_word_counts.most_common(nwords))

        # Step 6: Translate the most common words
        translations = translate_words(list(most_common_words.keys()), api_key)

        # Create a DataFrame to display the results
        df = pd.DataFrame(list(most_common_words.items()), columns=['Word', 'Count'])
        df['Translation'] = df['Word'].map(translations)

        # Put the count column first
        df = df[['Count', 'Word', 'Translation']]

        # Display the DataFrame
        print(df)

        # Save the DataFrame to a CSV file (check if the output file was provided)
        if not output_file:
            output_file = f'data/most_common/{language}.csv'

        df.to_csv(output_file, index=False)

    except Exception as e:
        print(f"An error occurred: {e}")

# Replace with the correct path to your PDF file and API credentials
file_path = 'data/harry_potter.pdf'

if __name__ == "__main__":
    args = parser.parse_args()
    file_path = args.file_path
    output_file = args.output
    nwords = args.nwords

    main(file_path, api_key, nwords, output_file)