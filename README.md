# LEARNPotter

![Logo](img/logo2.png){width=30%}

## Description

LEARNPotter is a small python project for learning new languages. The goal is to analyse
the first Harry Potter book and extract the most common words. The idea is to use this
list of words to learn a new language.

You will be able to choose the words you want to learn. The program will then generate
a list of sentences using these words. You can then use this list to learn the new language.

## Installation

````shell
git clone git@gitlab.com:lucasboistay/learn_potter.git
cd learn_potter
````

Create a virtual environment and install the dependencies:

- On Linux :

````shell
python3 -m venv .venv
.venv/bin/activate
````

- On Windows :

````shell
python -m venv .venv
.venv\Scripts\activate
````

Then install the dependencies:

````shell
pip install -r requirements.txt
````

### Project setup

By default, the project is set up to learn Portuguese. If you want to learn another language,
you will need to follow these steps.

Then you need to download the spacy model for the language you want to learn (You can find the list of available models [here](https://spacy.io/usage/models)).
For example, if you want to learn Portuguese, you can run the following command:

````shell
python -m spacy download pt_core_news_sm
````

Now, you will need to create an `API.py` file in the root of the project. This file
should contain your API key for DeepL (https://www.deepl.com/fr/your-account/keys), which is free
under 500'000 caracters per month. The file should contain the following code:

````python
# API.py
api_key = "YOUR_API_KEY"
````

You can now change the language you want to learn in the `constants.py` file. You just
have to load the correct spacy model and change the `SOURCE_LANG` and `TARGET_LANG` 
variables.

### Related Harry Potter words

As it is, this project still not filter automatically the words related to Harry Potter. Meaning
that you will need to find a list of words related to Harry Potter in the language you want to learn,
to remove them from the list. This is not necessary, but it can help you to focus on universal words in
the language you want to learn.

For now, you will need to create a CSV file named `[LANGUAGE_CODE].csv`, for example `pt.csv` for portuguese,
in the `data/related_words` folder. The file should contain
a list of words related to Harry Potter in the language you want to learn. 

### Project structure

At the end, the project should look like this:

````shell
project/
├── API.py
├── LICENSE
├── README.md
├── constants.py
├── data
│   ├── harry_potter.pdf
│   ├── most_common
│   │   └── PT.csv
│   └── related_words
│       └── pt.csv
├── main.py
├── requirements.txt
└── src
    ├── __init__.py
    ├── language_processing.py
    ├── pdf_processing.py
    └── translation.py
````

## Usage

You now need to download the Harry Potter book PDF in the desired language. When you have it,
rename it to `harry_potter.pdf` and put it in the `data` folder.

Then, you can run the following command:

````shell
python3 -m main [-h] [--file_path FILE_PATH] [--output OUTPUT] nwords
````

---

### Positional arguments:

- nwords : Number of most common words to display

### Options:
- -h, --help : show this help message and exit

- --file_path FILE_PATH : Path to the PDF file to analyze

- --output OUTPUT : Path to save the most common words CSV file

----

This will generate a list of the most common words in the book.
The list will be saved in the `data/most_common` folder.

## Support
If you have any issues, please create an issue on the repository (https://gitlab.com/lucasboistay/learn_potter/-/issues)

## Contributing

Contributing is more than welcome. You can fork the repository and create a merge request.

## Possible improvements

- [x] Add a translating feature (using DeepL)
- [ ] Find a way to get lists of Harry Potter related words in different languages automatically
- [ ] Add a nicer interface
- [ ] Get a list of all versions of the Harry Potter books in different languages
- [ ] Maybe link it to a Notion page in a Leitner system style

## License

License information can be found in the `LICENSE` file.