from langdetect import detect, LangDetectException
from collections import Counter

from constants import nlp

def load_harry_potter_words(language):
    try:
        with open(f'data/related_words/{language}.csv', 'r') as file:
            harry_potter_words = set(file.read().strip().lower().splitlines())
        return harry_potter_words
    except FileNotFoundError:
        raise ValueError(f"Harry Potter word list for language '{language}' not found.")

def detect_language(text):
    try:
        return detect(text)
    except LangDetectException as e:
        raise ValueError(f"Could not detect language: {e}")

def lemmatize_text(text, language):
    try:
        doc = nlp(text)
    except ValueError:
        raise ValueError(f"Language '{language}' not supported by spaCy")

    lemmatized_words = [token.lemma_ for token in doc]
    return ' '.join(lemmatized_words)

def clean_lemmatize_verify_words(word_counts, harry_potter_words):
    cleaned_word_counts = Counter()
    print("Cleaning, lemmatizing, and verifying words...")
    for word, count in word_counts.items():
        if word.isalpha() and word not in harry_potter_words:  # Remove non-alphabetic tokens and HP-specific words
            cleaned_word_counts[word] += count
    return cleaned_word_counts
