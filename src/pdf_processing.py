import fitz  # PyMuPDF
from tqdm import tqdm

def extract_text_from_pdf(file_path):
    try :
        pdf_document = fitz.open(file_path)
    except Exception as e:
        print(f"An error occurred: {e}")
        return ""
    text = ""
    print("Extracting text from the PDF...")
    for page_num in tqdm(range(len(pdf_document))):
        page = pdf_document.load_page(page_num)
        text += page.get_text("text") + " "
    return text
