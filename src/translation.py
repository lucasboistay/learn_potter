import requests
from tqdm import tqdm

from constants import SOURCE_LANG, TARGET_LANG

def translate_words(words, api_key):
    url = "https://api-free.deepl.com/v2/translate"
    headers = {
        "Authorization": f"DeepL-Auth-Key {api_key}"
    }
    translations = {}
    print("Translating words...")
    for word in tqdm(words):
        params = {
            "text": word,
            "source_lang": SOURCE_LANG,
            "target_lang": TARGET_LANG
        }
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 200:
            translations[word] = response.json()["translations"][0]["text"]
        else:
            print(f"Failed to translate {word}: {response.status_code}")
            translations[word] = word  # Fallback to the original word if translation fails
    return translations
