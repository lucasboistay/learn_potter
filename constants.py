import spacy

# Portuguese model
nlp = spacy.load("pt_core_news_sm")

SOURCE_LANG = "PT"
TARGET_LANG = "FR"